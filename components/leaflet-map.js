import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { latLngsAreEqual } from '../utils/map/geo-helpers'
import { arraysAreEqual } from '../utils/array-helpers'
import { Leaflet } from '../utils/map/leaflet'
import { FeatureType } from '../utils/map/feature-types'
import 'leaflet/dist/leaflet.css'

const undefinedOrTruthy = val => typeof val === 'undefined' || val

let map, postingFeatures

const featuresAreEqual = (f1, f2)  => f1.equals(f2)
const featuresChanged = (features, nextFeatures) => {
  return nextFeatures &&
    (!features || !arraysAreEqual(features, nextFeatures, featuresAreEqual))
}

const LatLngPropType = PropTypes.shape({
  lat: PropTypes.number,
  lng: PropTypes.number
})

const mapProps = {
  center: {
    onChange: (map, center) => map.panTo(center),
    changed: (props, nextProps) => {
      const center = props.center
      const nextCenter = nextProps.center
      return nextCenter && !latLngsAreEqual(center, nextCenter)
    },
    type: LatLngPropType
  },
  zoom: {
    onChange: (map, zoom) => map.setZoom(zoom),
    changed: (props, nextProps) => {
      const zoom = props.zoom
      const nextZoom = nextProps.zoom
      return nextZoom && zoom !== nextZoom
    },
    type: PropTypes.number
  },
  view: {
    onChange: (map, view) => map.setView(view.center, view.zoom),
    changed: (props, nextProps) => {
      /**
       * LIMITATION:
       * The view prop will only be considered changed if it is set to a value
       * different to the last value of the prop. It does not consider changes
       * to other props that may modify map center & zoom.
       */
      if (!nextProps.view) {
        return false
      } else {
        const { center, zoom } = props.view || {}
        const { center: nextCenter, zoom: nextZoom } = nextProps.view

        const zoomChanged = zoom !== nextZoom
        const centerChanged = !latLngsAreEqual(center, nextCenter)

        return zoomChanged || centerChanged
      }
    },
    type: PropTypes.shape({
      center: LatLngPropType,
      zoom: PropTypes.number
    })
  },
  bounds: {
    onChange: (map, bounds, props) => {
      const options = {}
      if (props.fitBoundsMaxZoom) {
        options.maxZoom = props.fitBoundsMaxZoom
      }
      map.fitBounds(bounds, options)
    },
    changed: (props, nextProps) => {
      const bounds = Leaflet.latLngBounds(props.bounds)
      const nextBounds = Leaflet.latLngBounds(nextProps.bounds)
      return nextBounds && nextBounds.isValid() &&
        (!bounds || !bounds.equals(nextBounds))
    },
    type: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ])
  },
  features: {
    onChange: (map, features) => {
      map.removeFeatures()
      if (features && features.length) {
        map.addFeatures(features)
      }
    },
    changed: (props, nextProps) => featuresChanged(props.features, nextProps.features),
    type: PropTypes.array
  },
  featuresToFit: {
    onChange: (map, features) => {
      const featureGroup = features.reduce((prev, curr) => {
        return prev.addLayer(Leaflet.geoJson(curr.getGeoJson()))
      }, new Leaflet.featureGroup())
      const bounds = featureGroup.getBounds()
      if (bounds.isValid()) map.fitBounds(bounds)
    },
    changed: (props, nextProps) => featuresChanged(props.featuresToFit, nextProps.featuresToFit),
    type: PropTypes.array
  }
}

const mapEvents = {
  featureclick: 'onClick',
  featuremouseout: 'onMouseOut',
  featuremouseover: 'onMouseOver',
  load: 'onLoad',
  unload: 'onUnload'
}

class LeafletMap extends Component {
  componentDidMount () {
    const options = {
      worldCopyJump: true,
      accessToken: this.props.accessToken,
      minZoom: this.props.minZoom,
      maxZoom: this.props.maxZoom,
      maxBounds: this.props.maxBounds
    }
    options.zoomControl = undefinedOrTruthy(this.props.zoomControl)
    map = Leaflet.map(this.refs.map, options)
    if (this.props.tileLayer) this.props.tileLayer.addTo(map)
    this.updateMapProps()

    map.on('load', () => {
      Leaflet.control.scale().addTo(map)
      if (this.props.onBoundsChanged) {
        this.props.onBoundsChanged(map.getBounds())
      }
    })
    map.on('moveend', () => {
      if (this.props.onBoundsChanged) {
        this.props.onBoundsChanged(map.getBounds())
      }
    })
    Object.keys(mapEvents).forEach(eventName => {
      const handler = this.props[mapEvents[eventName]]
      if (handler) map.on(eventName, handler)
    })
  }

  componentWillUnmount () {
    map.remove()
  }

  componentWillReceiveProps (props) {
    if (map) this.updateMapProps(this.props, props)
  }

  updateMapProps (props = {}, nextProps = this.props) {
    Object.keys(mapProps).forEach(key => {
      const mapProp = mapProps[key]
      if (mapProp.changed(props, nextProps)) {
        mapProp.onChange(map, nextProps[key], nextProps)
      }
    })
  }

  render () {
    return <div className='map-container' ref='map'/>
  }
}

LeafletMap.propTypes = Object.keys(mapProps)
  .reduce((prev, curr) => ({
    ...prev,
    [curr]: mapProps[curr].type
  }), {})

export default LeafletMap
