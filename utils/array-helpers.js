const prop = prop => item => item[prop]

export const groupBy = by => list => {
  if (typeof by !== 'function') by = prop(by)
  return list.reduce((obj, item) => {
    const key = by(item)
    if (!obj[key]) obj[key] = []
    obj[key].push(item)
    return obj
  }, {})
}

export const mapListToProp = by => list => {
  if (typeof by !== 'function') by = prop(by)
  return list.reduce((obj, item) => {
    obj[by(item)] = item
    return obj
  }, {})
}

export const arraysAreEqual = (a1, a2, compare = (a, b) => a === b) => {
  if (a1 === a2) {
    return true
  } else if (a1.length !== a2.length) {
    return false
  } else {
    for (let i = 0; i < a1.length; ++i) {
      if (!compare(a1[i], a2[i])) {
        return false
      }
    }
    return true
  }
}

export const moveElement = (arr, fromIndex, toIndex) => {
  let newArr = [ ...arr ]
  newArr.splice(fromIndex, 1)
  return [
    ...newArr.slice(0, toIndex),
    arr[fromIndex],
    ...newArr.slice(toIndex)
  ]
}

export const exclude = elements => el => !elements.includes(el)

export const addOrRemove = (arr, element) => {
  const index = arr.indexOf(element)
  if (index < 0) {
    arr.push(element)
  } else {
    arr.splice(index, 1)
  }
  return arr
}

/**
 * E.g.
 *   range(3)    => [ 0, 1, 2 ]
 *   range(3, 6) => [ 3, 4, 5, 6 ]
 */
export const range = (...args) => {
  const [ from, to ] = args.length === 2 ? args : [ 0, args[0] - 1 ]
  return Array(to - from + 1).fill().map((_, n) => n + from)
}

/**
 * Given an array of objects, find index in that array using provided callback
 * function and then extend the object at that index with obj.
 * See test for examples.
 */
export const findAndExtend = (arr, callback, obj) => {
  const index = arr.findIndex(callback)
  if (index > -1) {
    return [
      ...arr.slice(0, index),
      { ...arr[index], ...obj },
      ...arr.slice(index + 1)
    ]
  } else {
    return [ ...arr ]
  }
}
