export const pluralize = (entityName, num = 0) => num === 1 ? entityName : `${entityName}s`
