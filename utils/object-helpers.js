import { arraysAreEqual } from './array-helpers'

export const exclude = (obj, keys) => {
  return Object.keys(obj).reduce((acc, key) => {
    if (keys.includes(key))
      return acc
    else
      return { ...acc, [key]: obj[key] }
  }, {})
}

export const include = (obj, keys) => {
  return Object.keys(obj).reduce((acc, key) => {
    if (keys.includes(key))
      return { ...acc, [key]: obj[key] }
    else
      return acc
  }, {})
}

const prop = str => obj => obj[str]
const exists = value => value !== null && value !== undefined
const empty = value => !exists(value)
const blank = value => !exists(value) || value === ''
const present = value => !blank(value)
const maybe = (fn, value) => exists(value) ? fn(value) : null

export const safeProp = (obj, path, defaultValue = null) => {
  let value = path.split('.').reduce((obj, key) => maybe(prop(key), obj), obj)
  return exists(value) ? value : defaultValue
}

/* Simple function to check if objects are the same. This will return false for
 * objects that contain nested objects.
 */
export const objectsAreEqual = (obj1, obj2) => {
  const obj1Keys = Object.keys(obj1)
  const obj2Keys = Object.keys(obj2)

  const bothArrays = (val1, val2) => Array.isArray(val1) && Array.isArray(val2)

  if (obj1Keys.length !== obj2Keys.length) {
    return false
  } else {
    for (let k in obj1) {
      const [ val1, val2 ] = [ obj1[k], obj2[k] ]
      if (bothArrays(val1, val2)) {
        if (!arraysAreEqual(val1, val2)) return false
      } else if (val1 !== val2) {
        return false
      }
    }
    return true
  }
}

/*
 * Removes key/value pairs where the value is null, undefined or empty string.
 * Blank values may also be kept if `keepBlank` is `true`.
 */
export const compact = (obj, { keepBlank = false } = {}) => {
  return Object.keys(obj).reduce((acc, key) => {
    let value = obj[key]
    let keepKey = keepBlank ? exists(value) : present(value)
    return keepKey ? { ...acc, [key]: value } : acc
  }, {})
}

/*
 * Converts all values to strings, unless the value is empty, in which case it is returned as empty string. This
 * implementation is shallow and does not intelligently handle Arrays or Objects as values.
 */
export const stringifyValues = obj => {
  return Object.keys(obj).reduce((acc, key) => {
    let value = obj[key]
    return { ...acc, [key]: empty(value) ? '' : value.toString() }
  }, {})
}

/*
 * Compacts and then stringifies an object
 */
export const normalizeQuery = obj => compact(stringifyValues(obj))

