export default function (fn, threshold) {
  let lastCall = null

  return function (...args) {
    const now = Number(new Date())
    const delta = lastCall ? now - lastCall : null
    if (!delta || delta > threshold) {
      lastCall = now
      fn(...args)
    }
  }
}
