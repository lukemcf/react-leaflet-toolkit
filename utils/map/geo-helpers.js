const isDefined = value => value !== null && value !== undefined
const lat = latLng => isDefined(latLng.lat) ? Number(latLng.lat) : null
const lng = latLng => isDefined(latLng.lng) ? Number(latLng.lng) : null

export const latLngsAreEqual = (latLng1, latLng2) => {
  latLng1 = latLng1 || {}
  latLng2 = latLng2 || {}
  return lat(latLng1) === lat(latLng2) && lng(latLng1) === lng(latLng2)
}

export const formatLatLng = latLng => {
  return `${lat(latLng)},${lng(latLng)}`
}

// Convert from array in form [ lat, lng ] into an object { lat, lng }
export const arrayToLatLng = array => {
  return { lat: array[0], lng: array[1] }
}

// Convert from object in form { lat, lng } into array [ lat, lng ]
export const latLngToArray = latLng => {
  return [ lat(latLng), lng(latLng) ]
}

// Given a lat & lng, return a GeoJSON point
export const point = (lat, lng) => ({
  type: 'Point',
  coordinates: [ Number(lng), Number(lat) ]
})

// Given GeoJSON point geometry, return a latLng object
export const pointToLatLng = point => ({
  lat: point.coordinates[1],
  lng: point.coordinates[0]
})
