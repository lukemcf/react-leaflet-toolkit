import { FeatureType } from './feature-types'
import { objectsAreEqual } from '../object-helpers'
import { arraysAreEqual } from '../array-helpers'

const coordinatesEqual = (feature1, feature2) => {
  return arraysAreEqual(feature1.geometry.coordinates, feature2.geometry.coordinates)
}

let currentId = 0
export default class Feature {
  constructor (type, geometry, properties = {}) {
    this.guid = currentId++
    this.type = type
    this.geometry = geometry
    this.properties = properties
  }

  getGeoJson () {
    return {
      type: 'Feature',
      geometry: this.geometry,
      properties: {
        ...this.properties,
        type: this.type
      }
    }
  }

  equals (otherFeature) {
    if (this.geometry.type !== otherFeature.geometry.type) {
      return false
    } else if (!coordinatesEqual(this, otherFeature)) {
      return false
    } else {
      return objectsAreEqual(this.properties, otherFeature.properties)
    }
  }
}
