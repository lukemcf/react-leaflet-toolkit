import _Leaflet from 'leaflet'
import { FeatureType, getLayer } from './feature-types'
import { groupBy } from '../array-helpers'
const groupByType = groupBy('type')

let Leaflet = _Leaflet

const featureEvents = [ 'click', 'mouseover', 'mouseout' ]

// @param features: array of features (/utils/map/feature)
Leaflet.Map.prototype.addFeatures = function (features) {
  if (!this.features) this.features = {}
  const featureGroup = new Leaflet.featureGroup()

  const byType = groupByType(features)
  Object.keys(byType).forEach(type => {
    const layers = byType[type].map(f => {
      const l = getLayer(f)
      featureEvents.forEach(eventName => {
        l.addEventListener(eventName, e => {
          this.fire(`feature${eventName}`, { ...e, feature: f })
        })
      })
      return l
    })
    this.features[type] = (this.features[type] || []).concat(layers)
    layers.forEach(l => featureGroup.addLayer(l))
  })

  featureGroup.addTo(this)
  return featureGroup
}

// @param types: string|array of feature types (FeatureType)
Leaflet.Map.prototype.removeFeatures = function (types = Object.keys(FeatureType)) {
  const _types = typeof types === 'string' ? [ types ] : types
  const layers = _types.reduce((prev, curr) => {
    if (this.features && this.features[curr]) {
      return [ ...prev, ...this.features[curr] ]
    } else {
      return prev
    }
  }, [])
  layers.forEach(layer => {
    layer.clearAllEventListeners()
    this.removeLayer(layer)
  })
}

let stubLeaflet = stub => Leaflet = { ...Leaflet, ...stub }

export { Leaflet, stubLeaflet }
