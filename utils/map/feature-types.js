/**
 * Feature types should be each defined as an object consisting of a typeName
 * and a getLayer function.
 *
 * e.g.
 *
 * let types = [{
 *   typeName: 'POSTING',
 *   getLayer (feature) => {
 *     return Leaflet.geoJson(f.getGeoJson(), {
 *       // GeoJson options:
 *       // http://leafletjs.com/reference.html#geojson-options
 *     })
 *   }
 * }]
 */

let FeatureType = {}
let typesByName = {}

export const getFeatureType = () => FeatureType

export const setTypes = types => {
  FeatureType = types.reduce((prev, curr) => {
    return { ...prev, [curr.typeName]: curr.typeName }
  }, {})

  typesByName = types.reduce((prev, curr) => {
    return { ...prev, [curr.typeName]: curr }
  }, {})
}

const isValidType = typeName => Object.keys(FeatureType).includes(typeName)

const getLayer = feature => {
  if (!isValidType(feature.type)) {
    throw new Error('Invalid feature type: ', feature.type)
  } else {
    return typesByName[feature.type].getLayer(feature)
  }
}

export { FeatureType, getLayer }
