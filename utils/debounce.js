export default function (fn, delay) {
  let timeoutId
  return function () {
    clearTimeout(timeoutId)
    timeoutId = setTimeout(fn.bind(this, ...arguments), delay)
  }
}
