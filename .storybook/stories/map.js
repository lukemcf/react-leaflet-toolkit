import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import { Leaflet } from '../../utils/map/leaflet'
import LeafletMap from '../../components/leaflet-map'
import './map.scss'

const tileUrl = 'https://api.tiles.mapbox.com/styles/v1/{username}/{style_id}/tiles/{z}/{x}/{y}?access_token={accessToken}'
const attribution = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>'
const accessToken = 'pk.eyJ1IjoibHVrZW1jZiIsImEiOiJjajJ3bTY3angwMDlvMnFxbm4wdGFqNHR1In0.vdN4h_z1SUGamILmFh3HeQ' // TODO move to config

const tileLayer = Leaflet.tileLayer(tileUrl, {
  attribution: attribution,
  maxZoom: 18,
  accessToken: accessToken,
  username: 'luke',
  style_id: 'foo'
})

storiesOf('Map', module)
  .add('with no features', () => {
    const props = {
      center: { lat: -40.9006, lng: 174.8860 },
      zoom: 4,
      tileLayer: tileLayer
    }
    return <LeafletMap { ...props }/>
  })
