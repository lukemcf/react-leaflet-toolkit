import { configure } from '@kadira/storybook'

function loadStories() {
  require('./stories/map.js')
}

configure(loadStories, module)
