const path = require('path')

module.exports = {
  module: {
    loaders: [
      {
        test: /.s?css$/,
        loader: 'style!css',
        include: path.resolve(__dirname, '../')
      }, {
        test: /\.json$/,
        loader: 'json-loader',
        include: path.resolve(__dirname, '../')
      }, {
        test: /\.(png|gif|jpg)$/,
        loader: 'url-loader',
        include: path.resolve(__dirname, '../')
      }
    ]
  }
}
