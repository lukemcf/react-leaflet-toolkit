React Leaflet Toolkit
=====================

A React component/toolkit to show a map using the open-source [Leaflet](http://leafletjs.com/) Javasript library.

## Installation

1. yarn install
2. yarn start

