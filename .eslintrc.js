module.exports = {
  'ecmaFeatures': {
    'jsx': true,
    'modules': true
  },
  'env': {
    'browser': true,
    'node': true
  },
  'parser': 'babel-eslint',
  'rules': {
    'arrow-parens': ['error', 'as-needed'],
    'babel/new-cap': [ 'error', {
      newIsCapExceptions: [ 'Leaflet.marker', 'Leaflet.layerGroup', 'Leaflet.featureGroup' ]
    }],
    'babel/no-await-in-loop': 'warn',
    'eol-last': ['error', 'always'],
    'func-call-spacing': ['error', 'never'],
    'generator-star-spacing': ['error', {'before': false, 'after': true}],
    'jsx-quotes': ['error', 'prefer-single'],
    'keyword-spacing': ['error', { 'before': true, 'after': true }],
    'max-len': ['error', { code: 125, comments: 125 }],
    'object-shorthand': ['error', 'methods'],
    'quotes': ['error', 'single', { avoidEscape: true }],
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'react/react-in-jsx-scope': 'error',
    'semi': ['error', 'never'],
    'space-before-function-paren': ['error', 'always'],
    'strict': ['error', 'never']
  },
  'plugins': [
    'babel',
    'react'
  ]
}
